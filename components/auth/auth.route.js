var express = require('express');
var router = express.Router();
var passwordHash = require("password-hash");
const map_user = require('./../../helpers/map_user_req');
const jwt = require('jsonwebtoken');
const config = require('./../../config');
const authCtrl = require('./../auth/auth.controller');
const sender = require('./../../config/nodemailer.config');
const User = require('../../models/user.model');
const map_user_req = require('./../../helpers/map_user_req');

//routes defined here

router.route('/login')
    .post(authCtrl.login)
router.route('/register')
    .post(authCtrl.registerUser)
router.get('/user/check-user/:id', authCtrl.checkUserById)

router.get('/logout',authCtrl.logout);
router.post('/forgot-password', authCtrl.sendEmailToChangePassword);
router.get('/reset-password/:token', authCtrl.changePassword)

module.exports = router;