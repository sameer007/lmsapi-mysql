const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../../config');
const { options } = require('../../config/nodemailer.config');
const map_user_req = require('../../helpers/map_user_req');
//const User = require('../../models/user.model');
const {sender,createMail} = require('./../../config/nodemailer.config');
const dirname = './../';
const { Sequelize, DataTypes, Op } = require('sequelize');

const db = require('../../models')

/*
  * login user
  */
async function login(req, res, next) {
    console.log('form request here at login >>>', req.body);

    const mappedUserData = map_user_req({}, req.body);
    console.log('mappedUserData >>>', mappedUserData);

    try {
        const user = await db.user.findOne({
            where: {
                email: mappedUserData.email
            }
        });

        if (user) {
            console.log('loggedInUSer >>>', user);

            var isMatched = passwordHash.verify(req.body.password, user.password);
            if (isMatched) {
                console.log(user);
                var token = jwt.sign({ id: user.id, name: user.username, role: user.role }, config.jwtSecret);
                res.json({
                    user,
                    token
                });
            } else {
                console.log('password didnot matched');
                next({
                    msg: 'invalid password credentials'
                })
            }

        } else {
            next({
                msg: "invalid username credentials"
            })
        }
    } catch (error) {
        console.log('error >>', error);
        next({
            msg: "Something went wrong"
        })
    }

}
/*
  * logout user
  */
function logout(req, res, next) {
    req.user = null ;
    req.status(200).json({
        msg:'logged out successfully'
    });
}
async function checkUserById(req, res, next) {
    console.log('checkUserByUsername route');
    try {
        const user = await db.user.findOne({
            where: {
                id: req.params.id
            }
        });

        res.status(200).json(user);
    } catch (error) {
        next({
            msg: 'user not found'
        })
    }
}
/*
  * send email for password change
  */
async function sendEmailToChangePassword(req, res, next) {
    //console.log('req.??', req);

    try {
        var mappedUser = map_user_req({}, req.body);
        console.log('mappedUser>>', mappedUser);

        var userData = await db.user.findOne({
            where: {
                email: mappedUser.email
            }
        });

        if (userData) {
            //userData = map_user_req({},userData);
            console.log('user found', userData.id);

            var resetToken = require('randomstring').generate(25);
            var passwordResetExpiryTime = Date.now() + 1000 * 60 * 60 * 5;

            let emailContent = {};
            emailContent.name = userData.username;
            emailContent.email = req.body.email;
            emailContent.link = `http://localhost:3030/api/auth/reset-password/${resetToken}`;
            console.log('emailContent>>', emailContent);

            var mailBody = createMail(emailContent);

            const updatableUserData = {};

            updatableUserData.passwordResetExpiry = passwordResetExpiryTime;
            updatableUserData.passwordResetToken = resetToken
            console.log('user id ', userData.id);

            const [rowsAffected, is_updated] = await db.user.update(updatableUserData, {
                where: {
                    id: userData.id
                },
                returning: true,
            });
            console.log('is_updated', is_updated);

            if(is_updated){
                sender.sendMail(mailBody, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                });
            }else{
                next({
                    msg: 'User not found'
                });
            }
        } else {
            next({
                msg: 'Email doesnot exist'
            });
        }
    } catch (error) {
        next(error);
    }

}
/*
  * change password
  */
async function changePassword(req, res, next) {
    var token = req.params.token;

    try {
        var mappedUser = map_user_req({}, req.body);
        console.log('user found', new Date());

        const userData = await db.user.findOne({
            passwordResetToken: token,
            passwordResetExpiry: {
                [Op.gte]: new Date(),
            }
        });

        if (userData) {
            //console.log('user found', userData);
            const updatableUserData = {}

            updatableUserData.password = passwordHash.generate(req.body.password);
            
            // once password is reset erase the token and expiry time
            updatableUserData.passwordResetToken = null;
            updatableUserData.passwordResetExpiry = null;

            const [rowsAffected, is_updated] = await db.user.update(updatableUserData, {
                where: {
                    id: userData.id
                },
                returning: true,
            });

            if(is_updated){
                const updatedUserData = await db.user.findOne({
                    where: {
                        id: userData.id
                    }
                });
                res.json(userData);
            }else{
                next({
                    msg: 'User not found'
                });
            }
        } else {
            next({
                msg: 'Your account is not feasible for password reset yet. Please contact customer support.'
            });
        }
    } catch (error) {
        next(error);
    }
}
/*
  * register new user
  */
async function registerUser(req, res, next) {
    mappedUser = map_user_req({}, req.body);
    try {
        if (req.body.password === req.body.repassword) {
            //console.log('req.body>>', req.body);
            mappedUser.password = passwordHash.generate(req.body.password)
            //console.log('mappedUser >>', mappedUser);

            const newUser = await db.user.create(mappedUser);
            // User exists in the database now!
            //console.log(newUser instanceof db.user); // true
            //console.log(newUser); // "Jane"

            //console.log(newUser.email); // "Jane"
            if (newUser) {

                res.status(200).json(newUser);

            } else {
                return next({ msg: 'Something went wrong while registering user' });
            }
        } else {
            return next({ msg: 'Password Does not match' });
        }
    } catch (error) {
        console.log('error>>', error);
        return next({ msg: 'Something went wrong' });
    }

}

module.exports = {
    login,
    logout,
    checkUserById,
    sendEmailToChangePassword,
    changePassword,
    registerUser
}