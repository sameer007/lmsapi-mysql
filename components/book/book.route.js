const router = require('express').Router();
const BookCtrl = require('./book.controller');
const authorize = require('../../middlewares/authorize');


const multer = require('multer');
//var upload = multer({ dest: './uploads/' }); //although it is a simple usuage it is not sufficient
var myStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/book/')
    },
    filename: function (req, file, cb) {
        console.log('file is >>', file);
        cb(null, Date.now() + '-' + file.originalname);
    }
});
function filter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType != 'image') {
        req.fileError = true;
        cb(null, false)
    } else {
        cb(null, true)
    }
}
var upload = multer({
    storage: myStorage,
    fileFilter: filter
});

//routes defined here

router.get('/bookList', BookCtrl.findAll);
router.post('/addBook',upload.single('image'), BookCtrl.insert);
router.delete('/deleteBook/:id', BookCtrl.remove);
router.put('/editBook/:id',upload.single('image'), BookCtrl.update);

router.put('/activateBook/:id', BookCtrl.activateBook);
router.put('/inactivateBook/:id', BookCtrl.inactivateBook);

router.route('/search')
    .post(BookCtrl.searchByPost);


module.exports = router;