//to convert req.body to object
function map_book_req(book, bookDetails) {
    if (bookDetails.bookName) {
        book.bookName = bookDetails.bookName
    }
    if (bookDetails.bookType) {
        book.bookType = bookDetails.bookType
    }
    if (bookDetails.isbnNo) {
        book.isbnNo = bookDetails.isbnNo
    }
    if (bookDetails.bookPublisher) {
        book.bookPublisher = bookDetails.bookPublisher
    }
    if (bookDetails.bookAuthor) {
        book.bookAuthor = bookDetails.bookAuthor
    }
    if (bookDetails.subject) {
        book.subject = bookDetails.subject
    }
    if (bookDetails.rackNo) {
        book.rackNo = bookDetails.rackNo
    }
    if (bookDetails.bookQty) {
        book.bookQty = bookDetails.bookQty
    }
    if (bookDetails.description) {
        book.description = bookDetails.description
    }
    if (bookDetails.author) {
        book.author = bookDetails.author
    }
    if (bookDetails.publisher) {
        book.publisher = bookDetails.publisher
    }
    if (bookDetails.bookPrice) {
        book.bookPrice = bookDetails.bookPrice
    }
    if (bookDetails.returnTimeframeDays) {
        book.returnTimeframeDays = bookDetails.returnTimeframeDays
    }
    if (bookDetails.finePerDay) {
        book.finePerDay = bookDetails.finePerDay
    }
    if (bookDetails.image) {
        book.image = bookDetails.image
    }

    if(bookDetails.status){
        book.status = bookDetails.status
    }

    if (bookDetails.user) {
        book.user = bookDetails.user
    }
    if (bookDetails.discount == 'true' && bookDetails.discountType && bookDetails.discountValue) {
        book.discount = {
            status: bookDetails.discount,
            discountType: bookDetails.discountType,
            discountValue: bookDetails.discountValue
        } //object expected 
    }
    if (bookDetails.ratingPoint && bookDetails.ratingMsg) {
        let ratings = {
            point: bookDetails.ratingPoint,
            message: bookDetails.ratingMsg,
            user: bookDetails.user
        }
        book.reviews.push(ratings);
    }
    return book;
}
function insert(reqData) {  //create

    return new Promise(function (resolve, reject) {
        let newBook = new BookModel({});
        var mappedBook = map_book_req(newBook, reqData);
        mappedBook.save(function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        });
    });

}

function fetch(condition) { //read
        return BookModel.find(condition)
            .populate('user', {
                password: 0
            })
            .sort({
                _id: -1
            })
            .exec();
}

/**
 * 
 * @param {string} id 
 * @param {object} updatedData 
 * @returns promise
 */
function update(id, updatedData) {  //update
    console.log('updated data ', updatedData);
    return new Promise(function (resolve, reject) {
        BookModel.findById(id)
            .exec(function (err, book) {
                if (err) {
                    reject(err);
                }
                if (book) {
                    var oldImage = book.image;
                    var updatedBook = map_book_req(book, updatedData);

                    updatedBook.save(function (err, data) {
                        if (err) {
                            return reject(err);
                        }
                        resolve({ data, oldImage });
                    });
                } else {
                    reject({
                        msg: 'Book not found'
                    });
                }
            });
    })

}

function remove(id) {   //delete
    return new Promise(function (resolve, reject) {
        BookModel.findByIdAndRemove(id)
            .exec(function (err, removed) {
                if (err) {
                    return reject(err);
                }
                resolve(removed);
            })
    })
}

module.exports = {
    insert, fetch, update, remove,
    map_book_req
};
