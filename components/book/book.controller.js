const BookQuery = require('./book.query');
const fs = require('fs');

const db = require('../../models')

const path = require('path');

/*
  * insert new book
  */
async function insert(req, res, next) {
    try {
        console.log('req.body>>>', req.body);
        console.log('req.file>>>', req.file);
        if (req.fileError) {
            return next({
                msg: 'invalid file format from filter'
            })
        }
        if (req.file) {
            var mimeType = req.file.mimetype.split('/')[0];
            if (mimeType != 'image') {
                // fs.unlink(path.join(process.cwd(), 'uploads/images/' + req.file.filename), function (err, done) {
                //     if (err) {
                //         console.log('err ');
                //     } else {
                //         console.log('removed');
                //     }
                // })
                return next({
                    msg: "invalid file format"
                });
            }
        }
        var bookData = BookQuery.map_book_req({}, req.body);

        console.log('bookData>>', bookData);
        // data.user = req.user._id;
        if (req.file) {
            bookData.image = req.file.filename;
        }
        const newBook = await db.book.create(bookData);
        console.log('newBook>>', newBook);

        if (newBook) {

            res.status(200).json(newBook);

        } else {
            return next({ msg: 'Something went wrong while adding book' });
        }
    } catch (error) {
        next(error);
    }
}
/*
  * find all books
  */
async function findAll(req, res, next) {
    
    try {
        const allBookData = await db.book.findAll();

        if(allBookData){
            res.status(200).json(allBookData);

        }else{
            return next({ msg: 'Something went wrong while retreiving all books' });  
        }

    } catch (error) {
        next(error);
        
    }

}
/*
  * Update book info
  */
async function update(req, res, next) {

    try {
        console.log('req.file >>>', req.file);
        console.log('req.body >>', req.body);
        req.body.user = req.user._id;
        if (req.file) {
            req.body.image = req.file.filename;
        }
        const dataToUpdate = BookQuery.map_book_req({}, req.body);
        console.log('dataToUpdate>>', dataToUpdate);
        console.log('bookid>>', req.params.id);

        const [rowsAffected, is_updated] = await db.book.update(dataToUpdate, {
            where: {
                id: req.params.id
            },
            returning: true,
        });
        
        if (is_updated) {
            const updatedBookData = await db.book.findOne({
                where: {
                  id: req.params.id
                }
              });

            if(updatedBookData){
                res.status(200).json(updatedBookData);

            }else{
                return next({ msg: 'Something went wrong while getting updated book' });  
            }

        } else {
            return next({ msg: 'Something went wrong while updating book' });
        }
    } catch (error) {
        next(error);
    }

    // BookQuery.update(req.params.id, req.body)
    //     .then(function (response) {
    //         if (req.file) {
    //             fs.unlink(path.join(process.cwd(), 'uploads/images/' + response.oldImage), function (err, done) {
    //                 if (err) {
    //                     console.log('err');
    //                 } else {
    //                     console.log('removed');
    //                 }
    //             });
    //         }
    //         res.json(response.data);
    //     })
    //     .catch(function (err) {
    //         next(err);
    //     })
}
/*
  * change book status to active
  */
async function activateBook(req, res, next) {

    try {
        
        
        console.log('bookid>>', req.params.id);

        const [rowsAffected, is_updated] = await db.book.update({status : 1}, {
            where: {
                id: req.params.id
            },
            returning: true,
        });
        
        if (is_updated) {
            const updatedBookData = await db.book.findOne({
                where: {
                  id: req.params.id
                }
              });

            if(updatedBookData){
                res.status(200).json(updatedBookData);

            }else{
                return next({ msg: 'Something went wrong while getting updated book' });  
            }

        } else {
            return next({ msg: 'Something went wrong while activating book' });
        }
    } catch (error) {
        next(error);
    }
}
/*
  * change book status to inactive
  */
async function inactivateBook(req, res, next) {

    try {
        console.log('bookid>>', req.params.id);

        const [rowsAffected, is_updated] = await db.book.update({status : 0}, {
            where: {
                id: req.params.id
            },
            returning: true,
        });
        
        if (is_updated) {
            const updatedBookData = await db.book.findOne({
                where: {
                  id: req.params.id
                }
              });

            if(updatedBookData){
                res.status(200).json(updatedBookData);

            }else{
                return next({ msg: 'Something went wrong while getting deactivating book' });  
            }

        } else {
            return next({ msg: 'Something went wrong while deactivating book' });
        }
    } catch (error) {
        next(error);
    }
}
/*
  * delete existing book
  */
async function remove(req, res, next) {
    try {
        
        
        console.log('bookid>>', req.params.id);
        const deletableBookData = await db.book.findOne({
            where: {
              id: req.params.id
            }
          });

        if (deletableBookData) {
            const is_deleted = await db.book.destroy({
                where: { id: req.params.id }, // Specify the condition for deletion
              });

            if(is_deleted){
                
                    fs.unlink(path.join(process.cwd(), 'uploads/book/' + deletableBookData.image), function (err, done) {
                        if (err) {
                            console.log('err');
                        } else {
                            console.log('removed');
                        }
                    });
                
                res.status(200).json(deletableBookData);

            }else{
                return next({ msg: 'Something went wrong while deleting book data' });  
            }

        } else {
            return next({ msg: 'Book data not found for deletion' });
        }
    } catch (error) {
        next(error);
    }
}
/*
  * search book be post request
  */
async function searchByPost(req, res, next) {
    console.log('req.body>>', req.body);
    var condition = BookQuery.map_book_req({},req.body);
    try {
        const allBookData = await db.book.findAll(
            {where: condition}
        );

        if(allBookData){
            res.status(200).json(allBookData);

        }else{
            return next({ msg: 'Something went wrong while retreiving all books' });  
        }

    } catch (error) {
        next(error);
        
    }

}
/*
  * search book be get request
  */
function searchByGet(req, res, next) {
    var condition = {};
    var searchCondition = ItemQuery.map_item_req(condition, req.query);
    console.log('search condition >>', searchCondition);
    ItemQuery.fetch(searchCondition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    insert, findAll, findAll, update, remove, searchByGet, searchByPost,activateBook,inactivateBook
}

