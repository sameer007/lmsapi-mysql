let jwt = require('jsonwebtoken');
let config = require("./../config");
//const UserModel = require('../components/auth/user.model');

const db = require('../models');
const map_user_req = require('../helpers/map_user_req');
module.exports = function (req, res, next) {
    // console.log('req.headers >>>', req.headers);
    var token;
    //console.log('cookie', req.cookies.token);
    
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    else if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    else if (req.headers['token']) {
        token = req.headers['token']
    }
    else if (req.query.token) {
        token = req.query.token
    }else if (req.cookies.token){
        token = req.cookies.token
    }
    console.log('req.cookies>>',req.cookies);
    
    if (!token) {
        //res.redirect('/auth/login');

        return next({
            msg: "token not provided"
        })
    }
    jwt.verify(token, config.jwtSecret, async function (err, decoded) {
        if (err) {
            return next(err);
        }
        console.log('decoded value is >>>', decoded);
        // decoded value is not always identical to db value
        // if user is removed from system still token will work
        
        try {
            const user = await db.user.findOne({
                where: {
                    id: decoded.id
                }
            });
            if (user) {
                req.user = user;
                req.user.token = token;
                //console.log('flash at authenticate middleware>>',req.flash());
                //res.locals.msg = req.flash('info');
                return next();
            } else {
                next({
                    msg: "user removed from system"
                })
            }
        } catch (error) {
            console.log('error>>',error);
            next({
                msg: "Something went wrong"
            })
        }
        
        

    });
}