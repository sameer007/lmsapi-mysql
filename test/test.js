//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

var assert = require('assert');
chai.use(chaiHttp);


const token = `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwibmFtZSI6IlNhbWVlciIsInJvbGUiOjEsImlhdCI6MTY5MTU2ODEzOX0._K9AFikN0JKdK1w1RQs1eONEk6NMA25PfMyNgF0jtPY`
var defaultUser = {
    email: "sameeracharya_0@yahoo.com",
    password: "password",
    repassword: "password"
}

var defaultBook = {
    "bookName": "EPH",
    "bookType": "Academic",
    "description": "Environment Population and Health",
    "bookQty": 40,
    "isbnNo": "89111",
    "rackNo": "2",
    "author": "Saugat",
    "publisher": null,
    "subject": "health",
    "bookPrice": 600,
    "returnTimeframeDays": 9,
    "finePerDay": 20,
}
/*
  * Test the /POST insert new user route
  */
describe('/POST new user', () => {
    it('it should insert new user', (done) => {
        chai.request(server)
            .post('/auth/register')
            .send({
                email: defaultUser.email,
                password:defaultUser.password,
                repassword:defaultUser.repassword
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /POST login user route
  */
describe('/POST login user', () => {
    it('it should login', (done) => {
        chai.request(server)
            .post('/auth/login')
            .send({
                email: defaultUser.email,
                password: defaultUser.password, 
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /GET check user route
  */
describe('/GET check user by id', () => {
    it('it should CHECK USER BY ID', (done) => {

        chai.request(server)
            .get('/auth/user/check-user/4')
            .end((err, res) => {
                res.should.have.status(200);
                //console.log('data >>',res.json());
                done();
            });
    });
});
/*
  * Test the /POST new book route
  */
describe('/POST new book', () => {
    it('it should add new book', (done) => {

        chai.request(server)
            .post('/home/book/addBook')
            .set({ token: token })
            .send(defaultBook)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /PUT update book status route
  */
describe('/PUT book status', () => {
    it('it should change book status as deactivated', (done) => {

        chai.request(server)
            .put('/home/book/activateBook/1')
            .set({ token: token })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /PUT update book status route
  */
describe('/PUT update book status', () => {
    it('it should change book status as activated', (done) => {

        chai.request(server)
            .put('/home/book/inactivateBook/1')
            .set({ token: token })
            
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /PUT edit book route
  */
describe('/PUT edit book', () => {
    it('it should edit book data', (done) => {

        chai.request(server)
            .put('/home/book/editBook/1')
            .set({ token: token })
            .send({
                author: 'saguna'
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});

describe('/PUT edit book', () => {
    it('it should edit book data', (done) => {

        chai.request(server)
            .put('/home/book/editBook/1')
            .set({ token: token })
            .send({
                author: 'Saugat'
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /GET book list route
  */
describe('/GET book list', () => {
    it('it should get bookList', (done) => {

        chai.request(server)
            .get('/home/book/bookList')
            .set({ token: token })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /POST SEARCH book route
  */
describe('/POST SEARCH book', () => {
    it('it should search book', (done) => {

        chai.request(server)
            .post('/home/book/search')
            .set({ token: token })
            .send({
                isbnNo: defaultBook.isbnNo
            })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});
/*
  * Test the /DELETE book route
  */
describe('/DELETE book', () => {
    it('it should delete book', (done) => {

        chai.request(server)
            .delete('/home/book/deleteBook/3')
            .set({ token: token })
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });
});