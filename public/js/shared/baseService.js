function verifyUserLogin(){
    var CURRENT_ROUTE = getCurrentRoute();
    console.log(API_ROUTE+'/'+CURRENT_ROUTE);
    console.log('localStorage>>',localStorage);


    if(isLoggedIn()){
        var token = localStorage.getItem("token");
        var id = localStorage.getItem("id");
        if(id){
            fetchGET(API_ROUTE+'/home/dashboard/'+id,token,function(err,loggedInUserData){
            if(err){
                console.log('err>>',err);
                logout();
            }else{
                console.log('loggedInUserData>>',loggedInUserData);
                //console.log(API_ROUTE);
                var endUrl = getEndUrl();
                console.log(CURRENT_ROUTE);
                if(!loggedInUserData._id){
                    logout();
                }else{
                    if(CURRENT_ROUTE == 'auth'){
                        redirect('home/dashboard/'+loggedInUserData._id,'info','You are already logged in as '+loggedInUserData.username);
                    }else{
                        console.log('user login verified!')
                    }
                }
            }
        });
        }else{
            logout();
        }
    }else{
        logout();
        
    }
    
}