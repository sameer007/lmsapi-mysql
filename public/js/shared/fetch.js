async function fetchGET(url,token,cb) {
    console.log('tkenFEtchGet',token);
    if(token){
        option = {
            method: 'GET',
            headers: {
              'x-access-token': token || null
            },
          }
    }else{
        option = {
            method: 'GET'
          }
    }
    console.log('at fetch : GET');
    await fetch(url,option)
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            cb(null,data);
        })
        .catch(function(err) {
            cb(err,null);

        });
}

async function fetchPUT(url,formIdOrCondition,token,cb) {
    //console.log('url>>',url);
    //console.log('token>>',token);
    const postData = new URLSearchParams();
    if(typeof formIdOrCondition == 'string'){
        formId = formIdOrCondition;
        formElement = document.getElementById(formId);

        for (const pair of new FormData(formElement)) {
            postData.append(pair[0], pair[1]);
        }
    }else if(typeof formIdOrCondition == 'object'){
        var condition = formIdOrCondition;
        for (const key in condition) {
            postData.append(key, condition[key]);
        }
    }
    
    console.log('postData>>',postData.toString());
    
    if(token){
        option = {
            method: 'PUT', // or 'POST'
            headers: {
              'x-access-token': token || null
            },
            body: postData
          }
    }else{
        option = {
            method: 'PUT', // or 'POST'
            body: postData
          }
    }
    console.log('at fetch : PUT');
    await fetch(url,option)
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            cb(null,data);
        })
        .catch(function(err) {
            cb(err,null);

        });
}

async function fetchPOST(url,formId,token,cb) {
    //console.log('url>>',url);
    //console.log('token>>',token);
    formElement = document.getElementById(formId);

    const postData = new URLSearchParams();
    for (const pair of new FormData(formElement)) {
        postData.append(pair[0], pair[1]);
    }
    //console.log('postData>>',postData.toString());
    
    if(token){
        option = {
            method: 'POST', // or 'PUT'
            headers: {
              'x-access-token': token || null
            },
            body: postData
          }
    }else{
        option = {
            method: 'POST', // or 'PUT'
            body: postData
          }
    }
    console.log('at fetch');
    await fetch(url,option)
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            cb(null,data);
        })
        .catch(function(err) {
            cb(err,null);

        });
}

async function fetchDELETE(url,token,cb) {
    
    if(token){
        option = {
            method: 'DELETE',
            headers: {
              'x-access-token': token || null
            },
          }
    }else{
        option = {
            method: 'DELETE'
          }
    }
    console.log('at fetch : DELETE');
    await fetch(url,option)
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            cb(null,data);
        })
        .catch(function(err) {
            cb(err,null);

        });
}

