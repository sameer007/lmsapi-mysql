const API_ROUTE = 'http://localhost:9090/api';
function redirect(redirectUrl,notificationType = null,notification = null){
    var location = window.location.href;
    var arr = location.split('/');
    var baseUrl = arr[0]+'//'+arr[2]+'/';
    url = baseUrl+redirectUrl;
    if(notificationType){
      if(notificationType == 'success'){
        localStorage.setItem('notificationType','success');
      }else if(notificationType == 'warning'){
        localStorage.setItem('notificationType','warning');

      }else if(notificationType == 'danger'){
        localStorage.setItem('notificationType','danger');

      }else if(notificationType == 'info'){
        localStorage.setItem('notificationType','info');
      }
      if(notification){
        localStorage.setItem('notification',notification);
      }else{
        localStorage.removeItem('notificationType');
      }
    }
    window.location.replace(url);
  }

  function setNotification(){
    var notification = localStorage.getItem('notification');
    if(notification){
      localStorage.removeItem('notification');
      var notificationType = localStorage.getItem('notificationType');
      if(notificationType){
        localStorage.removeItem('notificationType');

        if(notificationType == 'success'){
          $('.alert.alert-success').html(notification);
          $('.alert.alert-success').removeClass('hidden');
        }else if(notificationType == 'warning'){
          $('.alert.alert-warning').html(notification);
          $('.alert.alert-warning').removeClass('hidden');
        }else if(notificationType == 'danger'){
          $('.alert.alert-danger').html(notification);
          $('.alert.alert-danger').removeClass('hidden');
        }else if(notificationType == 'info'){
          $('.alert.alert-info').html(notification);
          $('.alert.alert-info').removeClass('hidden');
        }
      }
    }

  }

  function getCurrentRoute(){
    var location = window.location.href;
    var arr = location.split('/');
    var currentRoute = arr[3];
    return currentRoute;
  }

  function getBaseUrl(){
    var location = window.location.href;
    var arr = location.split('/');
    var baseUrl = arr[0]+'//'+arr[2]+'/';
    return baseUrl;
  }

  function getEndUrl(){
    var location = window.location.href;
    var arr = location.split('/');
    var endUrl = arr[arr.length - 1];
    return endUrl;
  }

  function logout() {
    localStorage.clear();
    var CURRENT_ROUTE = getCurrentRoute();

    document.cookie = "_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    if(CURRENT_ROUTE != 'auth'){
      redirect('auth/login','danger','You have been logged out of system');
    }
  }

  function isLoggedIn() {
    return localStorage.getItem('token') ? true : false;
  }
  