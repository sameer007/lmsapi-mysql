function addBook() {
    $("#addBookForm").submit(function () {
        event.preventDefault();
        var url = API_ROUTE + '/book';
        var token = localStorage.getItem("token");
        console.log(API_ROUTE);
        fetchPOST(API_ROUTE + '/book', 'addBookForm', token, function (err, addedBookData) {
            if (err) {
                console.log('err>>', err);
            } else {
                console.log('addedBookDataHere>>', addedBookData);
                redirect('home/bookList', 'success', 'Book added Successfully');
            }
        });
    });
}

function updateBook() {
    $("#editBookForm").submit(function () {
        event.preventDefault();
        var url = API_ROUTE + '/book';
        var token = localStorage.getItem("token");
        console.log(API_ROUTE);
        bookId = window.location.search.split('?id=')[1];

        fetchPUT(API_ROUTE + '/book/' + bookId, { status: 1, data: 'b' }, token, function (err, updatableBookData) {
            if (err) {
                console.log('err>>', err);
            } else {
                console.log('updatableBookData>>', updatableBookData);
                redirect('home/bookList', 'success', 'Book updated Successfully');
            }
        });
    });
}
function getBookList() {
    var url = API_ROUTE + '/book';
    var token = localStorage.getItem("token");
    console.log(API_ROUTE);
    fetchGET(API_ROUTE + '/book', token, function (err, bookData) {
        if (err) {
            console.log('err>>', err);
        } else {
            console.log('bookDataHere>>', bookData);
            //$('#bookListTbody tr').remove();
            //console.log('bookListTrClone>>',bookListTrClone[0]);
            bookData.forEach(function (items, key) {
                var bookListTrClone = $('#bookListTbody tr:first-child').clone();
                var bookListTrCloneChildren = bookListTrClone.children();
                var bookListTbody = $('#bookListTbody');

                //console.log('key>>',key,'items>>',items);
                bookListTrCloneChildren.parent().attr('id', items._id);

                bookListTrCloneChildren.each(function (index, element) {
                    //console.log('this>>',index,element,element.class);

                    if (index == 0) {
                        element.innerHTML = items.bookName;
                    } else if (index == 1) {
                        element.innerHTML = items.bookType;

                    } else if (index == 2) {
                        element.innerHTML = items.description;

                    } else if (index == 3) {
                        element.innerHTML = items.isbnNo;

                    } else if (index == 4) {
                        element.innerHTML = items.rackNo;

                    } else if (index == 5) {
                        element.innerHTML = items.createdAt;

                    } else if (index == 6) {
                        element.innerHTML = items.updatedAt;

                    } else if (index == 7) {
                        if (items.status == 1) {
                            element.innerHTML = '<span class="label label-success label-mini">Not Due</span>';
                        } else {
                            element.innerHTML = '<span class="label label-danger label-mini">Due</span>';
                        }
                    } else if (index == 8) {

                    }
                });
                //console.log('bookListTrClone>>',bookListTrClone[0]);
                bookListTbody.append(bookListTrClone[0]);
            });
            $('#bookListTbody tr:first-child').remove();
        }
    });

}

function getBookById() {
    console.log('dsada>>',);
    bookId = window.location.search.split('?id=')[1];

    console.log('on getBookById');
    var url = API_ROUTE + '/book';
    var token = localStorage.getItem("token");
    console.log(API_ROUTE);
    fetchGET(API_ROUTE + '/book/' + bookId, token, function (err, bookData) {
        if (err) {
            console.log('err>>', err);
        } else {
            console.log('bookDataHere>>', bookData);
            if (bookData._id) {
                $('#bookName').val(bookData.bookName);
                $('#bookType').val(bookData.bookType);
                $('#isbn').val(bookData.isbnNo);
                $('#publisher').val(bookData.publisher);
                $('#author').val(bookData.author);
                $('#subject').val(bookData.subject);
                $('#rackNo').val(bookData.rackNo);
                $('#qty').val(bookData.bookQty);
                $('#price').val(bookData.bookPrice);
                $('#returnTimeframeDays').val(bookData.returnTimeframeDays);
                $('#finePerDay').val(bookData.finePerDay);
                $('#description').val(bookData.description);

            } else {
                redirect('home/bookList', 'danger', 'something went wrong while retreiving book data to edit');

            }
        }
    });
}

function deleteBook() {
    $(document).on('click', '.deleteBook', function () {
        //event.preventDefault();
        //event.stopPropagation();
        //event.stopImmediatePropagation();
        console.log('deleteBook onClick');

        if (confirm("Are you sure you want to delete this book?")) {
            bookId = $(this).parent().parent().attr('id');
            var url = API_ROUTE + '/book';
            var token = localStorage.getItem("token");
            console.log(API_ROUTE);
            fetchDELETE(API_ROUTE + '/book/' + bookId, token, function (err, deletedBookData) {
                if (err) {
                    console.log('err>>', err);
                } else {
                    console.log('DeletedBookDataHere>>', deletedBookData);
                    if (deletedBookData._id) {
                        redirect('home/bookList', 'success', 'Book \"' + deletedBookData.bookName + '\" Deleted Successfully');
                    } else {
                        redirect('home/bookList', 'danger', 'something went wrong while deleting book');

                    }
                }
            });
        }

    });
}

function editBook() {
    $(document).on('click', '.editBook', function () {
        //event.preventDefault();
        //event.stopPropagation();
        //event.stopImmediatePropagation();
        console.log('editBook onClick');
        //alert("Are you sure you want to delete this book?");
        bookId = $(this).parent().parent().attr('id');
        CURRENT_ROUTE = getCurrentRoute();
        var url = './bookList';
        //var token = localStorage.getItem("token");
        console.log(window.location.host + '/' + CURRENT_ROUTE + '/editBook?id=' + bookId);
        console.log('class>>', $(this).attr('class'));
        // fetchGET(API_ROUTE + '/book/' + bookId, token, function (err, bookData) {
        //     if (err) {
        //         console.log('err>>', err);
        //     } else {
        //         console.log('bookDataHere>>', bookData);
        //         if (bookData._id) {
        //             editBookUrl = 'home/editBook?id=' + bookData._id;

        //             redirect(editBookUrl, 'info', 'you can edit book here');
        //         } else {
        //             redirect('home/bookList', 'danger', 'cant find book in database');
        //         }
        //     }
        // });
    });
}

function updateBookStatus() {
    $(document).on('click', '.changeStatus', function () {
        console.log('changeStatus onClick');
        //alert("Are you sure you want to delete this book?");
        bookId = $(this).parent().parent().attr('id');
        CURRENT_ROUTE = getCurrentRoute();
        var token = localStorage.getItem("token");
        console.log('class>>', $(this).attr('class'));
        
        if (confirm('Are you sure you want to change book status?')) {
            fetchGET(API_ROUTE + '/book/' + bookId, token, function (err, updatableBookData) {
                if (err) {
                    console.log('err>>', err);
                } else {
                    if (updatableBookData._id) {
                        console.log('updatableBookDataHere>>', updatableBookData);

                        if (updatableBookData.status == 1) {
                            updatableData = { status: 0 };
                        } else {
                            updatableData = { status: 1 };
                        }
                        console.log('updatableData>>', updatableData);

                        fetchPUT(API_ROUTE + '/book/' + bookId, updatableData, token, function (err, updatedBookData) {
                            if (err) {
                                console.log('err>>', err);
                            } else {
                                console.log('updatedBookData>>', updatedBookData);
                                redirect('home/bookList', 'success', 'Book status updated Successfully');
                            }
                        });
                    } else {
                        redirect('home/bookList', 'danger', 'cant find book in database');
                    }
                }
            });
        }

    });
}
