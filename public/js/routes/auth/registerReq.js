function registerUser() {
  $("#registerForm").submit(function () {
    event.preventDefault();
    var url = API_ROUTE + '/auth/register';
    var token = null;
    console.log(API_ROUTE);
    fetchPOST(API_ROUTE + '/auth/register', 'registerForm', token, function (err, userData) {
      if (err) {
        console.log('err>>', err);
      } else {
        console.log('userDataHereRegister>>', userData);

        if (userData._id){
          redirect('auth/login', 'success', 'User registered successfully');

          // if (userData.token) {
          //   localStorage.setItem("token", userData.token);
          //   localStorage.setItem("id", userData.user._id);

          // }
          // var token = localStorage.getItem("token");

          // fetchGET(API_ROUTE + '/home/dashboard/' + userData._id, token, function (err, loggedInUserData) {
          //   if (err) {
          //     console.log('err>>', err);

          //   } else {
          //     console.log('loggedInUserData>>', loggedInUserData);
          //     console.log(API_ROUTE);
          //     //redirect('home/dashboard/' + loggedInUserData.id);

          //   }
          // });
        }else{
          redirect('auth/register', 'danger', userData.message);

        }

      }
    });
  });
}


