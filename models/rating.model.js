module.exports = (sequelize, DataTypes) => {

    const Rating = sequelize.define("rating", 
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        point: {
            type: DataTypes.INTEGER
        },
        message: {
            type: DataTypes.STRING
        }
    }, {
        timestamps: true
    })

    return Rating

}