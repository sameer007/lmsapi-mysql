module.exports = (sequelize, DataTypes) => {

    const User = sequelize.define("user", {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
        username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true
        },
        phoneNumber: {
            type: DataTypes.INTEGER
        },
        dob: {
            type: DataTypes.DATE
        },
        gender: {
            type: DataTypes.ENUM('male', 'female', 'others')
        },
        role: {
            type: DataTypes.INTEGER,
            defaultValue: 1
        },
        status: {
            type: DataTypes.BOOLEAN,
            defaultValue: true
        },
        passwordResetExpiry: {
            type: DataTypes.DATE
        },
        passwordResetToken: {
            type: DataTypes.STRING
        }
    }, {
        timestamps: true
    })

    return User

}