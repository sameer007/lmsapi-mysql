
module.exports = (sequelize, DataTypes) => {
    const Rating = require("./rating.model")(sequelize,DataTypes);

    const Book = sequelize.define("book", 
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        bookName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        bookType: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.STRING
        },
        bookQty: {
            type: DataTypes.INTEGER
        },
        isbnNo: {
            type: DataTypes.STRING
        },
        rackNo: {
            type: DataTypes.STRING
        },
        author: {
            type: DataTypes.STRING
        },
        publisher: {
            type: DataTypes.STRING
        },
        subject: {
            type: DataTypes.STRING
        },
        bookPrice: {
            type: DataTypes.FLOAT
        },
        returnTimeframeDays: {
            type: DataTypes.INTEGER
        },
        finePerDay: {
            type: DataTypes.FLOAT
        },
        image: {
            type: DataTypes.STRING
        },
        discountStatus: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        discountType: {
            type: DataTypes.ENUM('percentage', 'value', 'qty')
        },
        discountValue: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 1
        }
    }, {
        timestamps: true
    });

    Book.hasMany(Rating);
    Rating.belongsTo(Book);
    return Book

}