// db connections
const { HOST, USER, PASSWORD, DB, DIALECT } = require('../config/db.config');


const { Sequelize, DataTypes } = require('sequelize');

// connecting to mysql database
const sequelize = new Sequelize(DB, USER, PASSWORD, {
    host: HOST,
    dialect: DIALECT
});

(async () => {
    try {
        await sequelize.authenticate(); // Sync the models with the database
        console.log('Connection has been established successfully.');

    } catch (error) {
        console.error('Unable to connect to the database:', error);

    }
})();

const db = {};

db.Sequelize = Sequelize
db.sequelize = sequelize

db.user = require('./user.model')(sequelize,DataTypes);
db.book = require('./book.model')(sequelize,DataTypes);
db.rating = require('./rating.model')(sequelize,DataTypes);

(async () => {
    try {
        await db.sequelize.sync({force:false});

        console.log('Models Sync successful.');

    } catch (error) {
        console.error('Unable to Sync Models:', error);

    }
})();

module.exports = db ;
