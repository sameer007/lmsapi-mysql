const router = require('express').Router();
var authRoute = require("../components/auth/auth.route");
var homeRoute = require("./home.route");
var bookRouter = require('../components/book/book.route');
// load middlewares
const authenticate = require('./../middlewares/authenticate');
const authorize = require("./../middlewares/authorize");

//routes for api exposed here

module.exports = function () {
    router.use('/auth', authRoute)
    router.use('/home', authenticate, homeRoute);

    return router;

}