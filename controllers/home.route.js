var router = require('express').Router();
var map_user = require("../helpers/map_user_req");
var passwordHash = require('password-hash');
const authorize = require('../middlewares/authorize');
var bookRouter = require('../components/book/book.route');

router.use('/book', bookRouter);

router.route('/dashboard/:id')
    .get(function (req, res, next) {
        console.log('req.user >>>', req.user);

        // UserModel.findOne({ _id: req.params.id })
        //     .then(function (user) {
        //         res.status(200).json(user);
        //     })
        //     .catch(function (err) {
        //         next(err);
        //     })
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            res.status(200).json(user);
        })

    })





module.exports = router;